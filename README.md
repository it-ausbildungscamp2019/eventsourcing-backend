## SBB IT-Ausbildungscamp 2019 - "Hands-On Eventsourcing and CQRS using reactive programming"

### Content
Here you'll find the solutions for the main example (backend) using akka actors and streaming.

* `Supervisor` - root of actor hierarchy, bootstraps all other actors
* `InboundActor` - simulating a streaming command source (i.e. message queue or the like), here from a file
* `VerkehrsmittelActor` - persistent actor and aggregate root for "Verkehrsmittel" entities, it receives commands
  from inbound actor or from user, either refuses them if invalid or accepts them if ok. Accepted commands lead
  to events being generated and persisted into event store by this actor, moreover the events mutate the internal 
  state of this actor. That state can be restored by recovering all of the events from the store (while actor is restarting).
* `WebsocketActor` - handles the communication between a client and the backend, acting as a bridge which streams the
  events from backend to client and forwards user actions to the appropriate backend actor which can handle them.

