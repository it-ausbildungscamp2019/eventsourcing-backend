package ch.sbb.ausbildung.eventsourcing.backend.vm;

import akka.actor.Kill;
import akka.actor.Props;
import akka.event.LoggingAdapter;
import akka.persistence.AbstractPersistentActor;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor.Event.VerkehrsmittelCreated;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor.Event.VerkehrsmittelDelayed;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor.Event.VerkehrsmittelMoved;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import scala.Immutable;

import java.io.Serializable;

/**
 * Persistent actor which stores successful commands (messages) as events into the event store.
 */
public class VerkehrsmittelActor extends AbstractPersistentActor {

    private final LoggingAdapter log = context().system().log();

    private final State state;

    private VerkehrsmittelActor() {
        this.state = State.empty();
    }

    @Override
    public Receive createReceiveRecover() {
        return receiveBuilder()
                .match(VerkehrsmittelCreated.class, evt -> {
                    log.info("recover event: {}", evt);
                    this.eventHandler(evt);
                })
                .match(VerkehrsmittelMoved.class, evt -> {
                    log.info("recover event: {}", evt);
                    this.eventHandler(evt);
                })
                .matchEquals("complete", (req) -> self().tell(Kill.getInstance(), self()))
                .build();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Command.CreateVerkehrsmittel.class, this::commandHandler)
                .match(Command.MoveVerkehrsmittel.class, this::commandHandler)
                .match(Command.DelayVerkehrsmittel.class, this::commandHandler)
                .match(Command.GetState.class, this::commandHandler)
                .build();
    }

    private void commandHandler(Command.MoveVerkehrsmittel move) {
        // if position in command is not passed yet -> set it as new position
        // TODO: implement command + event handler for intent "MoveVerkehrsmittel"
    }

    private void commandHandler(Command.CreateVerkehrsmittel command) {
        // if "verkehrsmittel" isn't existing yet, go ahead
        // TODO: implement command + event handler for intent "CreateVerkehrsmittel"
    }

    private void commandHandler(Command.DelayVerkehrsmittel command) {
        if (state.containsVerkehrsmittel(command.getVmNummer())) {

            // save event in store
            persist(VerkehrsmittelDelayed.builder()
                    .vmNummer(command.getVmNummer())
                    .delay(command.getDelay() == 0 ? null : command.getDelay())
                    .build(), this::eventHandler);
        }
    }

    private void commandHandler(Command.GetState getState) {
        sender().tell(state.verkehrsmittel, self());
    }

    private void eventHandler(VerkehrsmittelCreated created) {
        // TODO: implement event handler "VerkehrsmittelCreated"
    }

    private void eventHandler(VerkehrsmittelMoved moved) {
        // TODO: implement event handler "VerkehrsmittelMoved"
    }

    private void eventHandler(VerkehrsmittelDelayed delayed) {
        state.verkehrsmitteDelayed(delayed);
    }

    @Override
    public String persistenceId() {
        return "vm";
    }

    public static Props props() {
        return Props.create(VerkehrsmittelActor.class, VerkehrsmittelActor::new);
    }



    public interface Command extends Immutable {
        @Value
        class CreateVerkehrsmittel implements Command {
            final int vmNummer;
            final String vmArt;
            final String bezeichnung;
            final List<String> fahrtpunkte;
        }

        @Value
        class MoveVerkehrsmittel implements Command {
            final int vmNummer;
            final String aktuellePosition;
        }

        @Value
        class DelayVerkehrsmittel implements Command {
            final int vmNummer;
            final int delay;
        }

        @Value(staticConstructor = "instance")
        class GetState implements Command {
        }

        @Value(staticConstructor = "instance")
        class NoOp implements Command {
        }
    }

    public interface Event extends Immutable, Serializable {
        @Value
        @Builder
        class VerkehrsmittelCreated implements Event {
            private final int vmNummer;
            @NonNull
            private final Verkehrsmittel verkehrsmittel;
        }

        @Value
        @Builder
        class VerkehrsmittelMoved implements Event {
            private final int vmNummer;
            @NonNull
            private final String aktuellePosition;
        }

        @Value
        @Builder
        class VerkehrsmittelDelayed implements Event {
            private final int vmNummer;
            private final Integer delay;
        }
    }

    // State of VerkehrsmittelActor
    static class State {

        Map<Integer, Verkehrsmittel> verkehrsmittel = HashMap.empty();

        boolean containsVerkehrsmittel(int vmNummer) {
            return verkehrsmittel.containsKey(vmNummer);
        }

        Option<Verkehrsmittel> verkehrsmittelByVmNummer(int vmNummer) {
            return verkehrsmittel.get(vmNummer);
        }

        void verkehrsmittelCreated(VerkehrsmittelCreated created) {
            // TODO: mutate internal state on behalf of "VerkehrsmittelCreated" event
        }

        void verkehrsmittelMoved(VerkehrsmittelMoved moved) {
            // TODO: mutate internal state on behalf of "VerkehrsmittelMoved" event
        }

        void verkehrsmitteDelayed(VerkehrsmittelDelayed delayed) {
            verkehrsmittelByVmNummer(delayed.getVmNummer()).map(vm -> vm.setDelay(delayed.getDelay()));
        }

        static State empty() {
            return new State();
        }
    }
}
