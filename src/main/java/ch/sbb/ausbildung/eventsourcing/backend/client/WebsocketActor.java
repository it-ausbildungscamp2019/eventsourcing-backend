package ch.sbb.ausbildung.eventsourcing.backend.client;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.TextMessage;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.PersistenceQuery;
import akka.persistence.query.journal.leveldb.javadsl.LeveldbReadJournal;
import akka.stream.Materializer;
import akka.stream.QueueOfferResult;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.SourceQueueWithComplete;
import ch.sbb.ausbildung.eventsourcing.backend.client.FSA.UserCommands;
import ch.sbb.ausbildung.eventsourcing.backend.client.FSA.UserFSA;
import ch.sbb.ausbildung.eventsourcing.backend.client.FSA.VerkehrsmittelFSA;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor.Command;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor.Event;
import lombok.Value;

import java.util.concurrent.CompletionStage;

/**
 * Websocket-Actor handling the 2-way websocket communication between backend and frontend.
 * Each new browser session will create a new instance of this actor.
 *
 * @param <T>
 */
public class WebsocketActor<T extends FSA> extends AbstractLoggingActor {

    private static final String USERNAME = "USERNAME";
    private static final String USERNAME_UNDEF = "<undefined>";

    private final Class<T> fsaClass;
    private final Materializer materializer;
    private final ActorRef vmActor;
    private LeveldbReadJournal readJournal;
    private SourceQueueWithComplete<Message> toClientQueue;

    static Props props(ActorRef vmActor, Materializer materializer) {
        return Props.create(WebsocketActor.class, () -> new WebsocketActor(vmActor, materializer));
    }

    private WebsocketActor(ActorRef vmActor, Materializer materializer) {
        this.vmActor = vmActor;
        this.fsaClass = (Class<T>) VerkehrsmittelFSA.class;
        this.materializer = materializer;
    }

    @Override
    public void preStart() {
        readJournal = PersistenceQuery.get(context().system())
                .getReadJournalFor(LeveldbReadJournal.class, LeveldbReadJournal.Identifier());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(TextMessage.class, this::receive)
                .match(RegisterOutgoingQueue.class, this::init)
                .match(ConnectionError.class, streamTerminated -> context().stop(self()))
                .build();
    }

    private void init(final RegisterOutgoingQueue command) {
        this.toClientQueue = command.getQueue();
    }

    // reply with a serialized FSA to the client
    private CompletionStage<Boolean> reply(final FSA action) {
        try {
            log().info("Reply to client: {}", action.getType());
            return toClientQueue
                    .offer(TextMessage.create(FSA.writeValueAsString(action)))
                    .thenApply(queueOfferResult -> queueOfferResult == QueueOfferResult.enqueued());
        } catch (final Exception e) {
            throw new WebsocketException("cannot send to client", e);
        }
    }

    private void actionHandler(final VerkehrsmittelFSA action) {
        log().debug("dispatching: {}", action);

        switch (action.getType()) {
            case UserCommands.LOAD_USERNAME:
                pushUsername();
                break;
           // TODO: implement the rest of the case statements (2 in total)
            default:
                reply(FSA.error("server_error", "can not find dispatcher for action: " + action, "type unknown"));
                break;
        }
    }

    private void receive(final TextMessage textMessage) {
        try {
            log().debug("receive: {}", textMessage.getStrictText());
            final T request = FSA.readValueFor(textMessage.getStrictText(), fsaClass);
            try {
                actionHandler((VerkehrsmittelFSA) request);
            } catch (final Exception e) {
                log().error(e, "error while running request: " + request);
                final String details = "execution failed. exception=" +
                        e.getClass().getSimpleName();
                reply(FSA.error("server_error", details, "execution error"));
            }
        } catch (final Exception e) {
            log().error(e, "cannot parse request");
            final String details = "cannot parse request: " +
                    textMessage.getStrictText() + ", exception=" +
                    e.getClass().getSimpleName();
            reply(FSA.error("server_error", details, "parsing error"));
        }
    }

    private void pushEvents() {
        // TODO: stream the events to the client using the "readJournal"
    }

    private void pushUsername() {
        this.reply(UserFSA.action(FSA.UserEvents.USERNAME_LOADED, System.getenv().getOrDefault(USERNAME, USERNAME_UNDEF)));
    }

    private void delayVerkehrsmittel(VerkehrsmittelFSA action) {
        this.vmActor.tell(new Command.DelayVerkehrsmittel((int) action.getMeta(), Integer.parseInt((String) action.getPayload())), self());
    }


    // command sent from client websocket when connection got lost
    @Value
    static class ConnectionError {
    }

    // initialize the outbound queue (websocket-actor -> client)
    @Value
    static class RegisterOutgoingQueue {
        SourceQueueWithComplete<Message> queue;
    }

    static class WebsocketException extends RuntimeException {
        WebsocketException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
