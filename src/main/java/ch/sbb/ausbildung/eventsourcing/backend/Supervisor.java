package ch.sbb.ausbildung.eventsourcing.backend;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import ch.sbb.ausbildung.eventsourcing.backend.client.HttpRoute;
import ch.sbb.ausbildung.eventsourcing.backend.in.InboundActor;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor;

// Supervisor and root of actor hierarchy
public class Supervisor extends AbstractLoggingActor {

    static Props props() {
        return Props.create(Supervisor.class, Supervisor::new);
    }

    @Override
    public void preStart() throws Exception {
        // create main actor handling all commands and events
        ActorRef vmActor = context().actorOf(VerkehrsmittelActor.props(), "vmActor");

        // start HTTP (bind port and routes) and listen for incoming streaming data
        ActorSystem system = context().system();
        new HttpRoute().bindHttp(ActorMaterializer.create(context()), system, vmActor);
        // finally start the inbound streaming actor
        context().actorOf(InboundActor.props(vmActor));

        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }
}
