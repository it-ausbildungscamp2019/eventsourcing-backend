package ch.sbb.ausbildung.eventsourcing.backend.vm;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.io.Serializable;

@Value
@Builder
@EqualsAndHashCode(of = "vmNummer")
public class Verkehrsmittel implements Serializable {
    private final int vmNummer;

    private final String vmArt;

    private final String bezeichnung;

    private final List<String> fahrtpunkte;
    @NonFinal
    private String aktuellePosition;

    @NonFinal
    private Integer delay;

    Option<String> getAktuellePosition() {
        return Option.of(aktuellePosition).map(String::toUpperCase);
    }

    Verkehrsmittel setAktuellePosition(String aktuellePosition) {
        this.aktuellePosition = aktuellePosition;
        return this;
    }

    Verkehrsmittel setDelay(Integer delay) {
        this.delay = delay;
        return this;
    }
}
