package ch.sbb.ausbildung.eventsourcing.backend.in;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.alpakka.file.javadsl.FileTailSource;
import akka.stream.javadsl.Sink;
import ch.sbb.ausbildung.eventsourcing.backend.vm.VerkehrsmittelActor;
import com.opencsv.CSVParser;
import io.vavr.collection.List;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.Duration;

// inbound actor listening for new commands that will arrive (simulate queue or so..)
public class InboundActor extends AbstractLoggingActor {

    private final String fileLocation = context().system().settings().config().getString("eventsourcing.command-file.path");
    private final Path filePath = FileSystems.getDefault().getPath(fileLocation);
    private final ActorMaterializer materializer = ActorMaterializer.create(context().system());
    private final ActorRef vmActor;
    private final CSVParser parser = new CSVParser();

    public static Props props(ActorRef vmActor) {
        return Props.create(InboundActor.class, () -> new InboundActor(vmActor));
    }

    public InboundActor(ActorRef vmActor) {
        this.vmActor = vmActor;
    }

    @Override
    public void preStart() throws Exception {
        // TODO: read or stream the commands from file "verkehrsmittel.csv" into the verkehrsmittel-actor

        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }

    private VerkehrsmittelActor.Command toCommand(String line) throws IOException {
        final String[] values = parser.parseLine(line);

        if (ArrayUtils.isNotEmpty(values)) {
            switch (values[0]) {
                case "CreateVerkehrsmittel":
                    if (ArrayUtils.getLength(values) == 5) {
                        return new VerkehrsmittelActor.Command.CreateVerkehrsmittel(Integer.parseInt(values[1]), values[2], values[3], List.of(StringUtils.split(values[4], ";")));
                    }
                    break;
                case "MoveVerkehrsmittel":
                    if (ArrayUtils.getLength(values) == 3) {
                        return new VerkehrsmittelActor.Command.MoveVerkehrsmittel(Integer.parseInt(values[1]), values[2]);
                    }
                    break;
                default:
            }
        }
        return VerkehrsmittelActor.Command.NoOp.instance();
    }
}
