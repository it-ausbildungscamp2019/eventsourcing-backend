package ch.sbb.ausbildung.eventsourcing.backend;

import akka.actor.ActorSystem;

public class MainApp {

    public static void main(final String[] args) {
        // bootstrapping actor system
        ActorSystem system = ActorSystem.create("eventsourcing");

        // start supervisor
        system.actorOf(Supervisor.props(), "supervisor");
    }
}
